package br.com.mastertech.imersivo.pagamentos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.mastertech.imersivo.pagamentos.model.Pagamento;
import br.com.mastertech.imersivo.pagamentos.service.PagamentoService;

@RestController
public class PagamentoController {

	@Autowired
	private PagamentoService pagamentoService;
	
	@GetMapping("/pagamentos/{cartao_id}")
	public List<Pagamento> buscaPagamentos(@PathVariable Long cartao_id) {
		return pagamentoService.buscaPagamentos(cartao_id);
	}
	
	@PostMapping("/pagamento")
	public Pagamento criaPagamento(@RequestBody Pagamento pagamento) {
		return pagamentoService.criarPagamento(pagamento);
	}
	
}
